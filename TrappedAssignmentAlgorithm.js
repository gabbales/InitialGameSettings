Assignment Algorithm: 

If Stage != Zero
	Randomly pick 2 stage4_Challenge
	Take whole and divide by 2.
		(mod by 2 and add extra to second group)
		Group 1: 
			Assign stage4_Challenge1 to this group
			Distribute Items for stage randomly across the rooms
		Group 2: 
			Assign stage4_Challenge2 to this group
			Distribute Items for stage randomly across the rooms


Alternative work: 
Assign stage1_task to each student. 
Break students into pairs/triplets -> Do this with stage3/4 in mind? 


Alternative work example: 24 is the most we are prepared for (for now....)!
Stage1: Individual Challenge, investigate room, open door based on written clue
Stage2: Pair Challenge: now communication is possible, open door based on clue/item in someone else's room
Stage3: Small Grouping Challenge: combine knowledge from small group to talk to an NPC
Stage4: Large Group Challenge: Acquire items/Clues across all groupings to achieve larger goal in one person's room (essentially Pair Challenge but further)

**Naming patterns for this game:
	Whole = everyone, used for last stage
	Group = 2, the determinant from the second-highest stage
	Groupings = 4
	[sub-grouping, for groups > 24 = 8], used for npc stage
	pairs/trips = 8 [ex-large: = 16]
	individuals = for each student
**How do I want to distribute off-ones?
	%%% --> I want to handle them in real time 
**How do I want to handle triplets? 

Handling Triplets: - two pair challenges
	|| A || B || C ||	-	(original state)
	|| A  | B || C ||
	|| A  | B |  C ||
	|| A    B |  C ||
	|| A    B |  C ||
Triplet Handling: 
Take 2 twin challenges and apply them across middle: 
	A-B (opens BC) and A-C (opens CD)


**How do I want to incorporate NPCs?
**What content should be put in the sub-grouping stage in the case of xL groups? 
	%%% --> mini scavenger hunts? 

Game Plan version 3: 
	Grouping Algorithm: defines stagenum and grouping

	Break all students into groupings: 
	n = 23: 
		keep doing div2, modSum 
		23/2 = 	11 mod 1
				11/2 = 	5 mod 1
						5/2 =	2 mod 1
								2/2 = 1 mod 0
	(first off, do this math) stg = 4, modSum = 3

	keep doing div2, modSum 
	16/2 = 	8 mod 0
			8/2 = 	4 mod 0
					4/2 =	2 mod 0 
							2/2 = 1 mod 0

	stg = 4, modSum = 3

	for n = 16
	2.2-2.2|2.2-2.2|2.2-2.2|2.2-2.2

	for n = 17
	2.2-3.2|2.2-2.2|2.2-2.2|2.2-2.2

	Room names now assigned by byte: aaaaa
	Room name: 
	a (individual name)
	a (pair/trip name)
	a (Sub-grouping name)
	a (Grouping Name)
	a (Group Name)

	Challenges are numeric

/**
Code

n = number of students
*/
var studentList = [];

function startGroups(n) {
	//TODO Instantiate Challenge Lists

	var stg = getStageCount(n,0)

	var list = [];
	//Pick whole challenge
	var wholeId = '5-1';

	for(int = 0; i < n; i++){
		//create userroom object
		student.id = wholeId; //assign whole group id to student
		//add userroom object to list

	}

	studentlist = assignGroups(n,stg,curr,list);
}

function assignGroups(n,stgNum,curr,list){
	//Add recursiveBase-case
	if(curr==1){
		//TODO
		//Iterate through list
			// retrieve 1-* challenge
			// Assign 1-* challenge to list[0];
		return list;
	}
	else {
		var n1 = n/2; //11
		var n2 = n/2 + n%2; //12

		//Pick 1a challenge
		//Remove 1a from challenge list
		var groupIdA = "4-1";

		//Pick 2a challenge
		//Remove 2a from challenge list
		var groupIdB = "4-2";

		var listA = [];
		var listB = [];

		for(int i = 0; i < list.length; i++){
			var student = list[i];

			if(i<n1){	
				student.ID = studentId + "." + groupIdA; 
				listA.add(student);
			} else {
				student.ID = studentId + "." + groupIdB; 
				listB.add(student);
			}
		}
		curr = curr - 1;
		listA = assignGroups(n,stgNum,curr,listA);
		listB = assignGroups(n,stgNum,curr,listB);

		//TODO combine the two lists
		list = listA + listB;

		return list;
	}
}

function getStageCount(n,stgNum) {
	//Recursive End Case"
	if(n==1){
		return stgNum;
	} else {
		getStageCount(n/2,stgNum+1)
	}
}